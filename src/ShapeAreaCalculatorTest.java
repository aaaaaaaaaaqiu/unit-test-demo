import org.testng.Assert;

import static org.testng.Assert.*;

public class ShapeAreaCalculatorTest {

    @org.testng.annotations.Test
    public void testSquareArea() {
        Assert.assertEquals(ShapeAreaCalculator.squareArea(5), 25.0, "正⽅形⾯积计算错误。");
    }

    @org.testng.annotations.Test
    public void testCircleArea() {
        // 圆的半径为1时，⾯积应该接近于π。容许的误差为0.001
        Assert.assertEquals(ShapeAreaCalculator.circleArea(1), Math.PI, 0.001,
                "圆形⾯积计算错误。");
    }

    @org.testng.annotations.Test
    public void testTriangleArea() {
        // 3, 4, 5形成直⻆三⻆形，⾯积应为6
        Assert.assertEquals(ShapeAreaCalculator.triangleArea(3, 4, 5), 6.0,
                0.001, "三⻆形⾯积计算错误。");
    }

    @org.testng.annotations.Test
    public void testTrapezoidArea() {
        // 梯形上底2，下底3，⾼4，⾯积应为10.0。容许的误差为0.001
        Assert.assertEquals(ShapeAreaCalculator.trapezoidArea(2, 3, 4), 10.0,
                0.001, "梯形⾯积计算错误。");
    }
}